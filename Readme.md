## Tranform this template project to a real application

* Copy folder to a new path
* Delete the .git folder
* Rename the project name into package.json
* run npm-install

## Configure git repo for the real application

* Create the new repo into git provider (Bitbucket, Github)
* run git init
* Link the project to the remote repo following the command suggested from the provider
* Do the first commit
* Push

* For React Native default documentation see [here](src/master/React-Native-Readme.md)
