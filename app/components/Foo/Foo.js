import React, { Component } from 'react';
import { Container, Button, Text } from 'native-base';

export class Foo extends Component {
  render() {
    return (
			<Container>
				<Button>
					<Text>Button</Text>
				</Button>
			</Container>
   	);
	}
}
